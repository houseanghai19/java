/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalproject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import static java.util.Map.entry;
import java.util.Random;
import java.util.Scanner;
import static javax.management.Query.value;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Lenovo
 */
public class FinalProject {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) { 
        // TODO code application logic here    
        
        //លំហាត់ទី១
        System.out.println("Lucky Draw!");
        
        
        String[]gameConsole= new String[]{"01.012548796.Somnang","02.015487963.Dara",
            "03.016587420.Chenda","04.0975648922.Nary","05.023548762.Kakada",
            "06.0885423158.Veha","07.011458976.Thyda","08.099654328.Saly",
            "09.08546821.Theara","10.088652315.Davin"};

      /* 
        double lucky=Math.random();
        lucky*=10;
        int indexLucky=(int) lucky;
        
       
        for(int i=0;i<3;i++){ 
             
           System.out.println(gameConsole[indexLucky]); 
        }
      */  
  
   
      // System.out.println("Name:"+Arrays.toString(gameConsole));
       
       Random r=new Random();
       int index=r.nextInt(gameConsole.length);

        for(int i=0;i<3;i++){ 
          System.out.println("Winner is :"+ gameConsole[index]);
        }
        
        
       //លំហាត់ទី២
       System.out.println("==========================");
       System.out.println("Mini Dictionary!");
       
       
       Map<String,String>dictionary=new HashMap();
       
       dictionary.put("Ant","ស្រមោច");
       dictionary.put("Apple","ផ្លៃប៉ោម");
       dictionary.put("Boy","ក្មេងប្រុស");
       dictionary.put("Bed","គ្រែ");
       dictionary.put("Cat","សត្វឆ្មា");
       dictionary.put("Care","បារម្ភ");
       dictionary.put("Dog","សត្វឆ្កែ");
       dictionary.put("Donkey","សត្វលា");
       dictionary.put("Elephent","សត្វដំរី");
       dictionary.put("Eagle","សត្វឥន្ត្រី");
       dictionary.put("Fish","ត្រី");
       dictionary.put("First","ដំបូង");
       dictionary.put("Go","ទៅ");
       dictionary.put("God","ព្រះជាម្ចាស់");
       dictionary.put("Home","ផ្ទះ");
       dictionary.put("Hit","វ៉ៃ");
       dictionary.put("Ice","ទឹកកក");
       dictionary.put("Island","កោះ");
       dictionary.put("Justice","យុត្តិធម៌");
       dictionary.put("Jungle","ព្រៃ");
       
       //System.out.println(dictionary.size());
       //System.out.println(dictionary.values());
      // System.out.println(dictionary.keySet());
       
       
       
       
       
       Scanner sc=new Scanner(System.in); 
            System.out.print("Search Word: ");
            String word=sc.nextLine();
            
       
        Pattern pattern = Pattern.compile(dictionary.get(word));
        Matcher matcher = pattern.matcher(dictionary.get(word));
        
        
            
         if(matcher.find()) {
           System.out.println(word+"="+dictionary.get(word));
        
          } else {
            System.out.println("Match not found");
          }

     
      /* 
        var flag=dictionary.containsKey(word);
       if(flag==true){
           System.out.println(dictionary.values());
       }
       else{
           System.out.println("No");
       }
*/
      
      //លំហាត់ទិ៣
      System.out.println("==========================");
      System.out.println("ទស្សន៍ទាយហ៊ុងស៊ុយស្លាកលេខរថយន្ត");
      
      System.out.print("Input your number:");
      int num=sc.nextInt();
      
      int sum=0;
      int rem=0;

          while(num>0){
              sum+=num%10;
              num/=10;
              if(num==0&&sum>9){
                  num=sum;
                  sum=0;
              }                                       
        }
          
     // System.out.println("Anser is "+sum);
      
      switch(sum){
          case 1: 
              System.out.println("លេខ1 : មានតំណែងខ្ពស់ មានពិភពផ្ទាល់ខ្លួន ជីវិតគ្រួសារមិនពាក់ព័ន្ធជាមួយ នរណា ធ្វើអ្វីតែម្នាក់ឯងជាមួយនឹងគ្រួសារ ។");
          break;
          case 2: 
              System.out.println("លេខ2 : សំដៅដល់ស្ត្រី សេចក្តីសុខ ភាពសប្បាយ មានដៃគួជួយជ្រោមជ្រែង នឹង នាំមកនូវភាពសុខស្រួលមកឲ្យរថយន្តនេះ ។");
          break;
          case 3: 
              System.out.println("លេខ3 : ជាលេខព្រះ សំដៅដល់ការចាប់ផ្តើមនូវជោគជ័យ និងផលកម្រៃផ្សេងៗ ប៉ុន្តែអាចសំដៅដល់ឧប្បត្តិហេតុផងដែរ ។");
          break;
          case 4: 
              System.out.println("លេខ4 : មាន ក្តីក្តាំ ឈ្លោះទាស់ទែង មានបញ្ហាមិនល្អ ។ ប៉ុន្តែ អ្នកអាចកែបញ្ហានេះ បាន ដោយនាំមកនូវបន្ទះមាសចំនួយ៣បន្ទះ បិទលើព្រះថែរក្សារថយន្ត ដើម្បីជាសិរីមង្គលដល់ រថយន្តឲ្យផុតគ្រោះភ័យ ។");
          break;
          case 5: 
              System.out.println("លេខ5 : សំដៅដល់ការជួសជុល ការជាប់គាំងរឿងប្រាក់កាស ប៉ុន្តែអាចកែបញ្ហា បាន ដោយយកផ្កាថ្វាយជំនាងឡាន រាល់ថ្ងៃកំណើតរបស់ខ្លួនជួយឲ្យរួចផុតគ្រោះថ្នាក់ និងកាន់ តែសំណាងល្អ ។");
          break;
          case 6: 
              System.out.println("លេខ6 : មានអ្នកជួយជ្រោមជ្រែងផ្តល់លុយកាក់ច្រើន មានភាពចម្រើនរុងរឿង រួច ផុតពីគ្រោះថ្នាក់ទាំងពួង ។");
          break;
          case 7: 
              System.out.println("លេខ7 : សំដៅដល់ ការបាត់បង់ប្រាក់ ដោយសារការធ្វើដំណើរ ប៉ុន្តែអាចកែខៃ បាន ដោយយកផ្កាមកថ្វាយព្រះក្នុងរថយន្ត ឬជំនាងរថយន្ត នៅរាល់ថ្ងៃកំណើតរបស់អ្នក នឹងជួយ ឲ្យមានប្រាក់ច្រើន និងរួចផុតពីគ្រោះថ្នាក់ ។");
          break;
          case 8: 
              System.out.println("លេខ8 : សំដៅដល់ភាពរុងរឿង មានទ្រព្យសម្បត្តិច្រើន ទទួលបានជោគជ័យក្នុង ការងារ និងសុខភាពល្អបរិបូណ៌ ។");
          break;
          default: 
              System.out.println("លេខ9 : សំដៅដល់ជោគជ័យ មានភាពឈានទៅមុខ មានសេចក្តីសុខ និងមាន សុវត្ថិភាពល្អ ៕");
          break;
      }
      
    }
   
    
   
    
}
